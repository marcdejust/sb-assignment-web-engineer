SB Assignment Web Engeneer
==============


### Instructions

The purpose of this assessment is to complete a simple programming assignment.
You are expected to work on this task on your own, without help or advice from others. 
If you need clarification on any aspect of the assessment, please seek help from your organiser.

Please download and complete the coding assignment by 28th feb. 2022


### Coding Assignment

- Create a responsive product listing page React application like the image example 'image-example.png'.
- On desktop and tablet (viewport > 768px) display 3 columns.
- On mobile (viewport < 768px) dispolay 2 columns.
- Each column will have an image, a name, a description and a category name.
- Image, name, description and category should come from the [API](https://fakestoreapi.com/products).
- The code should be well structured and suitably commented.

